from django.shortcuts import render

# Create your views here.
response = {}
def index(request):
    response = {'author' : 'Samuel'}
    return render(request, 'lab_6/lab_6.html', response)
